package main

import (
	"GoRestAPI_CouchDB/common/database"
	router2 "GoRestAPI_CouchDB/pkg/router"
	"github.com/gin-gonic/gin"
)

func main() {
	database.CreateBulkInsert() // insert data from api to couchdb
	database.CreateDesignDoc()  // create design document for find doc by _rev
	database.CreateDesMapRed()  // create design document MapReduce for filter by classname

	router := gin.New()            //initualized gin package
	router2.RegisterRoutes(router) //register rout
	router.Run(":8081")            //run app on port 8081
}
