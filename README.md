
# Sang Sokea Homework02




### 1. find student by _id
```http
  GET localhost:8081/api/v1/student/findbyid/0907d50510a4045c4f5d836f12041a56
```

### 2. find student by _rev
```http
  GET localhost:8081/api/v1/student/findbyrev/1-ccbda58e1f479966d51eab1700aa4284
```

### 3. update student by _id and _rev
```http
  PUT localhost:8081/api/v1/student?id=0907d50510a4045c4f5d836f12042439&rev=1-82e789215f5757bdda1b179937e64c77
```
```http
body
  {
        "studentid": 1,
        "studentname": "Sang Sokea update",
        "gender": "M",
        "info": "",
        "is_class_monitor": false,
        "classname": "SR",
        "teamname": "DataAnalytics"
    }
```

### 4. delete student by _id and _rev
```http
  DELETE localhost:8081/api/v1/student?id=0907d50510a4045c4f5d836f12041a56&rev=1-ccbda58e1f479966d51eab1700aa4284
```

### 5. filter students by classname
#### Note: please use < classname=SR > not < classname="SR" >
```http
  GET localhost:8081/api/v1/student?classname=SR
```




## e. Why do we need to have a REST API for an existing API of CouchDB?

​ មូលហេតុដែលយើងត្រូវមាន REST API សម្រាប់ API ដែលមានស្រាប់របស់ CouchDB មានដូចខាងក្រោម
- REST API ផ្ដល់អោយយើងនូវភាព flexible ជាង API របស់​ CouchDB
- យើងអាច implment security layers ផ្សេងៗទៀតជាមួយនឹង REST API ជាជាងអោយគេ access ផ្ទាល់ជាមួយ database (CouchDB) របស់យើង ដែលអាចបណ្ដាលអោយមានបញ្ហាផ្សេងៗ
- Code ដែលយើងសរសេរនូវលើ REST API អាច​ reusable នឹង​ portable 
- ងាយប្រើជាមួយនឹង Web, App នឹងអាចអោយយើង integrate ជាមួយនឹង companies ផ្សេងៗទៀត
- ងាយស្រួលក្នុងការកំណត់​ permission នៃ functions មួយចំនួនតាមរយះ REST API



