package router

import (
	"GoRestAPI_CouchDB/controllers"
	"github.com/gin-gonic/gin"
)

func RegisterRoutes(r *gin.Engine) {
	router := r.Group("api/v1/student/") //group of request endpoint
	router.GET("findbyid/:id", controllers.FindDocById)
	router.GET("findbyrev/:rev", controllers.FindDocByRevId)
	router.DELETE("", controllers.DeleteDoc)
	router.PUT("", controllers.UpdateDoc)
	router.GET("", controllers.FindDocClassname)
}
