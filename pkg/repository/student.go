package repository

import (
	"GoRestAPI_CouchDB/common/model"
	"encoding/json"
	"io"
	"log"
	"net/http"
)

//return all student records from api
func GetStudents() model.AllStudents {
	var response model.AllStudents

	url := "https://api.jsonbin.io/v3/b/63155201a1610e63861e772b"
	res, err := http.Get(url)

	if err != nil {
		log.Fatal(err)
	}

	data, _ := io.ReadAll(res.Body)

	if err = json.Unmarshal(data, &response); err != nil {
		log.Fatal(err)
	}

	return response
}
