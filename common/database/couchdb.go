package database

import (
	"GoRestAPI_CouchDB/common/model"
	"GoRestAPI_CouchDB/pkg/repository"
	"context"
	"fmt"
	_ "github.com/go-kivik/couchdb/v4" // The CouchDB driver
	"github.com/go-kivik/kivik/v4"
)

var client, err = kivik.New("couch", "http://admin:1234567890@35.184.11.30:5984/")

func CreateBulkInsert() {
	err := client.CreateDB(context.TODO(), "studentdb")
	if err != nil {
		return
	}
	if err != nil {
		panic(err)
	}
	fmt.Println("Connected to", client.DSN())

	DB := client.DB("studentdb")

	listStudent := make([]interface{}, len(repository.GetStudents().Record))
	for i, v := range repository.GetStudents().Record {
		listStudent[i] = v
	}

	rev, err := DB.BulkDocs(context.TODO(), listStudent)
	if err != nil {
		panic(err)
	}
	fmt.Printf("student recode inserted with reversion %v \n", rev)
}

//Find a document by id
func FindDocById(docId string) (model.Student, error) {
	db := client.DB("studentdb")
	row := db.Get(context.TODO(), docId)
	if err != nil {
		panic(err)
	}
	var std model.Student
	if err = row.ScanDoc(&std); err != nil {
		return std, err
	}
	fmt.Printf("The std says '%s'\n", std.StudentName)
	return std, nil
}

//Find a document by revision number
func FindRevById(revId string) (model.Student, error) {
	db := client.DB("studentdb")
	rows := db.Query(context.TODO(), "_design/dstudent", "_view/getDocById", kivik.Options{
		"key":          revId,
		"include_docs": true,
	})

	var doc model.Student
	for rows.Next() {
		if err := rows.ScanDoc(&doc); err != nil {
			return doc, err
		}
		/* do something with doc */
		fmt.Printf("%#v", doc)
	}
	return doc, nil
}

//create design document
func CreateDesignDoc() {
	db := client.DB("studentdb")
	put, err := db.Put(context.TODO(), "_design/dstudent", map[string]interface{}{
		"_id": "_design/dstudent",
		"views": map[string]interface{}{
			"getDocById": map[string]interface{}{
				"map": "function(doc) { emit(doc._rev, doc) }",
			},
		},
	})
	if err != nil {
		return
	} else {
		fmt.Printf("successful create design document %v\n", put)
	}
}

//delete document by id and rev
func DeleteDoc(docId string, rev string) (string, error) {
	db := client.DB("studentdb")
	newRev, err := db.Delete(context.TODO(), docId, rev)
	if err != nil {
		return "", err
	}
	fmt.Printf("The tombstone document has revision %s\n", newRev)
	return "Document deleted", nil
}

//update doc by id and rev
func UpdateDoc(docId string, newStudent model.Student) (string, error) {
	db := client.DB("studentdb")
	fmt.Println("id in database ", docId)
	newRev, err := db.Put(context.TODO(), docId, newStudent)
	if err != nil {
		return "", err
	}
	fmt.Printf("The tombstone document has revision %s\n", newRev)
	return newRev, nil
}

//Design document with MapReduce to filter students by Classroom
func CreateDesMapRed() {
	db := client.DB("studentdb")
	put, err := db.Put(context.TODO(), "_design/dclass", map[string]interface{}{
		"_id": "_design/dclass",
		"views": map[string]interface{}{
			"getDocByclass": map[string]interface{}{
				"map":    "function(doc) { emit(doc.classname, doc) }",
				"reduce": "function (keys, values, rereduce) {if (rereduce) {return sum(values); } else {return values.length;}}",
			},
		},
	})
	if err != nil {
		return
	} else {
		fmt.Printf("successful create design document %v\n", put)
	}
}

var Studentlist []model.Student

func FindDocByClass(classname string) ([]model.Student, error) {
	db := client.DB("studentdb")
	rows := db.Query(context.TODO(), "_design/dclass", "_view/getDocByclass", map[string]interface{}{
		"key":          classname,
		"reduce":       false,
		"include_docs": true,
	})

	var doc model.Student

	for rows.Next() {
		if err := rows.ScanDoc(&doc); err != nil {
			fmt.Printf("FindDocByClass error: \n", err)
			return nil, err
		}
		/* do something with doc */
		Studentlist = append(Studentlist, doc)
	}
	fmt.Printf("this doc get by class %v\n", Studentlist)
	return Studentlist, nil
}
