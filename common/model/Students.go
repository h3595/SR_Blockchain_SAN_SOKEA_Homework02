package model

type Student struct {
	Id             string `json:"_id,omitempty"`
	Rev            string `json:"_rev,omitempty"`
	StudentId      int    `json:"studentid"`
	StudentName    string `json:"studentname"`
	Gender         string `json:"gender"`
	Info           string `json:"info"`
	IsClassMonitor bool   `json:"is_class_monitor"`
	Classname      string `json:"classname"`
	TeamName       string `json:"teamname"`
}
