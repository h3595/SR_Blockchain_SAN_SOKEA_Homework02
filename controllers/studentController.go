package controllers

import (
	"GoRestAPI_CouchDB/common/database"
	"GoRestAPI_CouchDB/common/model"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

//find doc by id
func FindDocById(c *gin.Context) {
	id := c.Param("id")
	fmt.Println("here is id:", id)
	student, err := database.FindDocById(id)
	if err != nil {
		c.JSON(http.StatusNotFound, err)
	} else {
		fmt.Println("here is student respone: ", student)
		c.JSON(http.StatusOK, &student)
	}

}

//find doc by id
func FindDocByRevId(c *gin.Context) {
	id := c.Param("rev")
	fmt.Println("here is id:", id)
	student, err := database.FindRevById(id)
	if err != nil {
		c.JSON(http.StatusNotFound, err)
	} else {
		fmt.Println("here is student respone: ", student)
		c.JSON(http.StatusOK, &student)
	}
}

//delete doc by id and rev
func DeleteDoc(c *gin.Context) {
	id := c.DefaultQuery("id", "")
	rev := c.Query("rev") // shortcut for c.Request.URL.Query().Get("lastname")
	fmt.Printf("Here is id = %s rev = %s", id, rev)
	student, err := database.DeleteDoc(id, rev)
	if err != nil {
		c.JSON(http.StatusNotFound, err)
	} else {
		fmt.Println("here is student respone: ", student)
		c.JSON(http.StatusOK, student)
	}
}

//update doc by id and rev
func UpdateDoc(c *gin.Context) {
	id := c.DefaultQuery("id", "")
	rev := c.Query("rev")
	var stud model.Student
	err := c.BindJSON(&stud)
	fmt.Println("Here is body ", stud)
	fmt.Println("Here is id ", id)
	stud.Rev = rev
	if err != nil {
		c.JSON(http.StatusNotFound, err)
	}
	newRev, err := database.UpdateDoc(id, stud)
	if err != nil {
		fmt.Println(err)
		c.JSON(http.StatusNotFound, err)
	} else {
		fmt.Println("here is student respone: ", newRev)
		doc, _ := database.FindRevById(newRev)
		c.JSON(http.StatusOK, doc)
	}
}

//get doc by classname
func FindDocClassname(c *gin.Context) {
	classname := c.Query("classname")
	fmt.Println("here is classname:", classname)
	if classname == "" {
		c.JSON(http.StatusBadRequest, "string query classname must be include in url")
	}
	student, err := database.FindDocByClass(classname)
	if err != nil {
		c.JSON(http.StatusNotFound, err)
	} else {
		fmt.Println("here is student respone: ", student)
		c.JSON(http.StatusOK, &student)
	}
	database.Studentlist = nil // clear array after request
}
